FROM node

EXPOSE 3000

ENV MONGO_HOST gds-mongo
ENV MONGO_PORT 27017
#GDS_ENV should be prod/dev/local
ENV GDS_ENV prod

WORKDIR /code
COPY package.json ./
RUN npm install pm2@latest -g && \
    npm install;

ADD . /code

CMD ./bin/start.sh
