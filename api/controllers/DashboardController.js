var flash500 = require("../services/flash500");
var Utils = require("../services/utils");
var moment = require("moment");

module.exports = {
  getDiseases: function (req, res) { return buildDiseases(req, res, "Diseases"); },
  getSymptoms: function (req, res) { return buildSymptoms(req, res, "Symptoms"); },
  getSurveys: function (req, res) { return buildSurveys(req, res, "Surveys"); },
  getProfiles: function (req, res) { return buildProfiles(req, res, "Profiles"); },
  getSummary: function(req, res) { return buildSummary(req, res, "Summary"); }
};

// getDiseases

function buildDiseases(req, res, identifier) {
  informRequest(identifier);

  Disease.find().populateAll().exec(function (err, diseases) {
    if (err) {
      return exposeError(req, res, err, identifier);
    }

    adjusted_diseases = adjustDiseases(diseases);

    informSuccess(identifier);

    return res.json({error: false, data: adjusted_diseases});
  });
}

function adjustDiseases(diseases) {
  var adjusted_diseases = {};

  for(var i = 0; i < diseases.length; i++) {
    var disease = diseases[i];
    adjusted_diseases[disease.code] = adjustDisease(disease);
  }

  return adjusted_diseases;
}

function adjustDisease(disease) {
  var adjusted_disease = {};

  adjusted_disease.name = disease.name;
  adjusted_disease.symptoms = adjustDiseaseSymptoms(disease);

  return adjusted_disease;
}

function adjustDiseaseSymptoms(disease) {
  var adjusted_symptoms = {};

  var symptoms = disease.symptoms;

  if(symptoms.length > 0) {
    for(var i = 0; i < symptoms.length; i++) {
      var symptom = symptoms[i];
      adjusted_symptoms[symptom.code] = symptom.name;
    }
  } else {
    adjusted_symptoms.empty = true;
  }

  return adjusted_symptoms;
}

// getSymptoms

function buildSymptoms(req, res, identifier) {
  informRequest(identifier);

  Symptom.find().populateAll().exec(function (err, symptoms) {
    if (err) {
      return exposeError(req, res, err, identifier);
    }

    adjusted_symptoms = adjustSymptoms(symptoms);

    informSuccess(identifier);

    return res.json({error: false, data: adjusted_symptoms});
  });
}

function adjustSymptoms(symptoms) {
  var adjusted_symptoms = {};

  for(var i = 0; i < symptoms.length; i++) {
    var symptom = symptoms[i];
    adjusted_symptoms[symptom.code] = adjustSymptom(symptom);
  }

  return adjusted_symptoms;
}

function adjustSymptom(symptom) {
  var adjusted_symptom = {};

  adjusted_symptom.name = symptom.name;
  adjusted_symptom.diseases = adjustSymptomDiseases(symptom);

  return adjusted_symptom;
}

function adjustSymptomDiseases(symptom) {
  var adjusted_diseases = {};

  var diseases = symptom.owners;

  if(diseases.length > 0) {
    for(var i = 0; i < diseases.length; i++) {
      var disease = diseases[i];
      adjusted_diseases[disease.code] = disease.name;
    }
  } else {
    adjusted_diseases.empty = true;
  }

  return adjusted_diseases;
}

// getSurveys

function buildSurveys(req, res, identifier) {
  informRequest(identifier);

  Survey.find().populateAll().sort({createdAt: "desc"}).exec(function (err, surveys) {
    if (err) {
      return exposeError(req, res, err, identifier);
    }

    adjusted_surveys = adjustSurveys(surveys);

    informSuccess(identifier);

    return res.json({error: false, data: adjusted_surveys});
  });
}

function adjustSurveys(surveys) {
  var adjusted_surveys = {};

  for(var i = 0; i < surveys.length; i++) {
    var survey = surveys[i];
    adjusted_surveys[survey.id] = adjustSurvey(survey);
  }

  return adjusted_surveys;
}

function adjustSurvey(survey) {
  var adjusted_survey = {};

  adjusted_survey.createdAt = adjustSurveyCreatedAt(survey.createdAt);
  adjusted_survey.isGoodReport = adjustSurveyNoSymptom(survey.no_symptom);
  adjusted_survey.lat = survey.lat;
  adjusted_survey.lon = survey.lon;
  cities_latlngs = buildCitiesLatLngs();
  adjusted_survey.state = adjustSurveyState(survey, cities_latlngs);
  adjusted_survey.province = adjustSurveyProvince(survey, cities_latlngs);
  adjusted_survey.city = survey.city;
  adjusted_survey.formattedAddress = adjustSurveyFormattedAddress(survey.formattedAddress);
  adjusted_survey.report = buildSurveyReport(survey, adjusted_survey.isGoodReport);
  var user = survey.user;
  if(user != null) {
    adjusted_survey.userID = user.id;
    adjusted_survey.profile = buildSurveyProfile(survey);
  } else {
    console.log("[ALERT] Missing user in Survey with ID:" + survey.id);
  }

  return adjusted_survey;
}

function buildCitiesLatLngs() {
  var cities = {};
  var lima = {};
  lima.state = "Lima";
  lima.lat0 = -11.85;
  lima.lon0 = -77.20;
  lima.lat1 = -12.29;
  lima.lon1 = -76.90;
  cities["Lima"] = lima;
  var trujillo = {};
  trujillo.state = "La Libertad";
  trujillo.lat0 = -8.06;
  trujillo.lon0 = -79.16;
  trujillo.lat1 = -8.18;
  trujillo.lon1 = -78.91;
  cities["Trujillo"] = trujillo;
  var puertoMaldonado = {};
  puertoMaldonado.state = "Madre de Dios";
  puertoMaldonado.lat0 = -12.55;
  puertoMaldonado.lon0 = -69.245;
  puertoMaldonado.lat1 = -12.65;
  puertoMaldonado.lon1 = -69.15;
  cities["Puerto Maldonado"] = puertoMaldonado;
  return cities;
}

function adjustSurveyCreatedAt(created_at) {
  var colombia_date = new Date(created_at)
  colombia_date.setHours(colombia_date.getHours() - 5);
  return colombia_date;
}

function adjustSurveyNoSymptom(no_symptom) {
  var is_good_report = false;
  if(no_symptom == "Y") {
    is_good_report = true;
  }
  return is_good_report;
}

function adjustSurveyState(survey, cities_latlngs) {
  var adjusted_state = "N/A"
  if(survey.state != null) {
    adjusted_state = survey.state;
  } else {
    var keys = Object.keys(cities_latlngs);
    for(var i = 0; i < keys.length; i++) {
      var city = keys[i];
      if(survey.lat <= cities_latlngs[city].lat0 && survey.lat >= cities_latlngs[city].lat1 &&
        survey.lon >= cities_latlngs[city].lon0 && survey.lon <= cities_latlngs[city].lon1) {
        adjusted_state = cities_latlngs[city].state;
        break;
      }
    }
  }
  return adjusted_state;
}

function adjustSurveyProvince(survey, cities_latlngs) {
  var adjusted_province = null;
  var keys = Object.keys(cities_latlngs);
  for(var i = 0; i < keys.length; i++) {
    var city = keys[i];
    if(survey.lat <= cities_latlngs[city].lat0 && survey.lat >= cities_latlngs[city].lat1 &&
      survey.lon >= cities_latlngs[city].lon0 && survey.lon <= cities_latlngs[city].lon1) {
      adjusted_province = city;
      break;
    }
  }
  if(adjusted_province == null) {
    if(survey.province != null) {
      adjusted_province = survey.province;
    } else {
      adjusted_province = "N/A";
    }
  }
  return adjusted_province;
}

function adjustSurveyFormattedAddress(formatted_address) {
  var adjusted_formatted_address = "N/A";
  if(formatted_address != null) {
    adjusted_formatted_address = formatted_address;
  }
  return adjusted_formatted_address;
}

function buildSurveyReport(survey, is_good_report) {
  var report = {};
  var symptoms = {};
  if(is_good_report) {
    report.hadHealthCare = "N/A";
    report.hadContagiousContact = "N/A";
    report.hadTravelledAbroad = "N/A";
    report.originCountry = "N/A";
    report.originProvince = "N/A";
    report.originState = "N/A";
    report.originCity = "N/A";
    report.startDate = "N/A";
    symptoms.empty = true;
  } else {
    report.hadHealthCare = survey.hadHealthCare != null ? survey.hadHealthCare ? "Sí" : "No" : "No";
    report.hadContagiousContact = survey.hadContagiousContact != null ? survey.hadContagiousContact ? "Sí" : "No" : "No";
    report.hadTravelledAbroad = survey.hadTravelledAbroad != null ? survey.hadTravelledAbroad ? "Sí" : "No" : "No";
    report.startDate = survey.startDate;
    if(report.hadTravelledAbroad) {
      report.originCountry = survey.originCountry;
      report.originState = survey.originState;
      report.originProvince = survey.originProvince;
      report.originCity = survey.originCity;
    } else {
      report.originCountry = "N/A";
      report.originState = "N/A";
      report.originProvince = "N/A";
      report.originCity = "N/A";
    }
    symptoms = buildSurveySymptoms(survey);
  }
  report.symptoms = symptoms;
  return report;
}

function buildSurveySymptoms(survey) {
  var symptoms = JSON.parse(JSON.stringify(survey));
  delete symptoms.user;
  delete symptoms.household;
  delete symptoms.no_symptom;
  delete symptoms.lat;
  delete symptoms.lon;
  delete symptoms.coordinates;
  delete symptoms.hadHealthCare;
  delete symptoms.hadConatigousContact;
  delete symptoms.hadTravelledAbroad;
  delete symptoms.travelLocation;
  delete symptoms.token;
  delete symptoms.originCountry;
  delete symptoms.originState;
  delete symptoms.originCity;
  delete symptoms.createdAt;
  delete symptoms.updatedAt;
  delete symptoms.app_token;
  delete symptoms.platform;
  delete symptoms.deviceModel;
  delete symptoms.ip;
  delete symptoms.startDate;
  delete symptoms.week_of;
  delete symptoms.exantematica;
  delete symptoms.diarreica;
  delete symptoms.respiratoria;
  delete symptoms.country;
  delete symptoms.state;
  delete symptoms.city;
  delete symptoms.zip;
  delete symptoms.formattedAddress;
  delete symptoms.id;
  return symptoms;
}

function buildSurveyProfile(survey) {
  var is_household = survey.household != null;
  var profile = null;
  if(is_household) {
    var profile = JSON.parse(JSON.stringify(survey.household));
    delete profile.app_token;
    delete profile.user;
    profile.age = getAge(profile.dob);
    profile.ageGroup = getAgeGroup(profile.age);
  } else {
    var profile = JSON.parse(JSON.stringify(survey.user));
    profile.relationship = "N/A";
    delete profile.active;
    delete profile.app;
    delete profile.email;
    delete profile.formattedAddress;
    delete profile.gcmTokens;
    delete profile.gcm_token;
    delete profile.isAdmin;
    delete profile.lastLogin;
    delete profile.lastSurvey;
    delete profile.token;
    delete profile.week_of;
  }
  delete profile.platform;
  delete profile.firstname;
  delete profile.lastname;
  delete profile.picture;
  delete profile.createdAt;
  delete profile.updatedAt;
  return profile;
}

function getAge(dob) {
  var diff_in_ms = Date.now() - new Date(dob);
  var diff_in_date = new Date(diff_in_ms);
  return Math.abs(diff_in_date.getUTCFullYear() - 1970);
}

function getAgeGroup(age) {
  var ageGroup = "";
  if(age < 13) {
      ageGroup = "00_12";
  } else if(age <= 19) {
      ageGroup = "13_19";
  } else if(age <= 29) {
      ageGroup = "20_29";
  } else if(age <= 39) {
      ageGroup = "30_39";
  } else if(age <= 49) {
      ageGroup = "40_49";
  } else if(age <= 59) {
      ageGroup = "50_59";
  } else if(age <= 69) {
      ageGroup = "60_69";
  } else if(age <= 79) {
      ageGroup = "70_79";
  } else {
      ageGroup = "80";
  }
  return ageGroup;
}

// getProfiles

function buildProfiles(req, res, identifier) {
  informRequest(identifier);

  User.find().exec(function (err, users) {
    if (err) {
      return exposeError(req, res, err, identifier + "(User)");
    }

    adjusted_profiles = adjustUsers(users);

    Household.find().exec(function (err2, households) {
      if (err) {
        return exposeError(req, res, err2, identifier + "(Household)");
      }

      Object.assign(adjusted_profiles, adjustHouseholds(households));

      informSuccess(identifier);

      return res.json({error: false, data: adjusted_profiles});
    });
  });
}

function adjustUsers(users) {
  var adjusted_users = {};

  for(var i = 0; i < users.length; i++) {
    var user = users[i];
    adjusted_users[user.id] = adjustUser(user);
  }

  return adjusted_users;
}

function adjustUser(user) {
  var adjusted_user = {};

  adjusted_user.name = user.name;
  adjusted_user.age = user.age;
  adjusted_user.ageGroup = user.ageGroup;
  adjusted_user.city = user.city;
  adjusted_user.province = user.province;
  adjusted_user.state = user.state;
  adjusted_user.country = user.country;
  adjusted_user.dob = user.dob;
  adjusted_user.gender = user.gender;
  adjusted_user.race = user.race;

  return adjusted_user;
}

function adjustHouseholds(households) {
  var adjusted_households = {};

  for(var i = 0; i < households.length; i++) {
    var household = households[i];
    adjusted_households[household.id] = adjustHousehold(household);
  }

  return adjusted_households;
}

function adjustHousehold(household) {
  var adjusted_household = {};

  adjusted_household.name = household.name;
  adjusted_household.age = buildHouseholdAge(household.dob.getTime());
  adjusted_household.ageGroup = buildHouseholdAgeGroup(adjusted_household.age);
  adjusted_household.city = household.city;
  adjusted_household.province = household.province;
  adjusted_household.state = household.state;
  adjusted_household.country = household.country;
  adjusted_household.dob = household.dob;
  adjusted_household.gender = household.gender;
  adjusted_household.race = household.race;
  adjusted_household.user = household.user;
  adjusted_household.relationship = household.relationship;

  return adjusted_household;
}

function buildHouseholdAge(birth_date_time) {
  var diff_in_ms = Date.now() - birth_date_time;
  var diff_in_date = new Date(diff_in_ms);
  var age = Math.abs(diff_in_date.getUTCFullYear() - 1970);
  return age;
}

function buildHouseholdAgeGroup(age) {
  var ageGroup = "";
  if(age < 13) {
      ageGroup = "00_12";
  } else if(age <= 19) {
      ageGroup = "13_19";
  } else if(age <= 29) {
      ageGroup = "20_29";
  } else if(age <= 39) {
      ageGroup = "30_39";
  } else if(age <= 49) {
      ageGroup = "40_49";
  } else if(age <= 59) {
      ageGroup = "50_59";
  } else if(age <= 69) {
      ageGroup = "60_69";
  } else if(age <= 79) {
      ageGroup = "70_79";
  } else {
      ageGroup = "80";
  }
  return ageGroup;
}

// getSummary

function buildSummary(req, res, identifier) {
  informRequest(identifier);

  User.count().exec( function countCB(err, quantity_of_users) {
    if (err) {
      return exposeError(req, res, err, identifier + "(User)");
    }
    var summary = {};
    summary.quantity_of_users = quantity_of_users;

    Household.count().exec(function countCB(err2, quantity_of_households) {
      if (err2) {
        return exposeError(req, res, err2, identifier + "(Household)");
      }
      summary.quantity_of_households = quantity_of_households;

      HealthUnit.count().exec(function countCB(err3, quantity_of_health_units) {
        if (err3) {
          return exposeError(req, res, err3, identifier + "(HealthUnit)");
        }
        summary.quantity_of_health_units = quantity_of_health_units;
        Symptom.count().exec(function countCB(err4, quantity_of_symptoms) {
          if (err4) {
            return exposeError(req, res, err4, identifier + "(Symptom)");
          }
          summary.quantity_of_symptoms = quantity_of_symptoms;
          informSuccess(identifier);
          return res.json({error: false, data: summary});
        });
      });
    });
  });
}

// Informs

function informRequest(identifier) {
  console.log("-> Dashboard " + identifier + " Requested");
}

function exposeError(req, res, error, identifier) {
  console.log("<- Dashboard " + identifier + " Request Failed: " + error)
  return flash500(req, res, {
    error: true,
    message: identifier + " request failed: " + err
  });
}

function informSuccess(identifier) {
  console.log("<- Dashboard " + identifier + " Request Succeeded")
}
