/**
 * HealthUnit
 *
 * @description :: Server-side logic for managing healthUnit
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
  /**
   * `HealthUnitController.delete()`
   */
  delete: function (req, res) {
    console.log("delete function Health Unit Controller");
    var redirectTo = '/health_units'
    var healthUnitId = req.param("health_unit_id");
    var client = req.param("client") || 'dashboard';
    HealthUnit.destroy({
      id: healthUnitId
    })
    .exec(function (err) {
      if (err){
        return flash500(req, res, {
          error: 'There was an error processing your request:',
          message: JSON.stringify(err)
        });
      }else{
        return res.clientAwareResponse(client, redirectTo, {
          status: true,
          message: "Health Unit Deleted"
        });
      }
    });
  },

  /**
   * `HealthUnitsController.index()`
   */
  index:(req, res) => {
    console.log("index function Health Units Controller");

    HealthUnit.find({}).sort({order: 'asc'})
    .exec((err, healthUnits) => {
      if(err){
        console.log("Error in index function Health Units Controller");
        return flash500(req, res, {
          error: 'There was an error processing your request:',
          message: JSON.stringify(err)
        });
      }else{
        console.log("Success in index function Health Units Controller");
        return res.view('healthUnits/health_unit_index' ,{
          healthUnits: healthUnits,
          error: 'false',
          page: 'health_unit_index'
        });
      }
    });
  },

  /**
   * `HealthUnitsController.list()`
   */
  list: function (req, res) {
    console.log("list function Health Units Controller");

    HealthUnit.find({}).sort({order: 'asc'})
    .exec(function (err, healthUnit) {
      if(err){
        return flash500(req, res, {
          error: 'There was an error processing your request:',
          message: JSON.stringify(err)
        });
      }else{
        console.log("list function HealthUnit Controller: ", err, healthUnit);
        return res.json({
          error: 'false',
          data: healthUnit
        });
      }
    });
  },

  /**
   * `HealthUnitsController.parse()`
   *
   */

  parse: function (req, res) {
    console.log("Parse function Health Units");
    req.file('file').upload({
      maxBytes: 10000000
    }, (err, uploadedFiles) => {
      var fs = require('fs');
      fs.readFile(uploadedFiles[0].fd, 'utf-8', (err, data) => {
        var lines = data.split('\n');
        for(var line = 0; line < lines.length; line++){
          var info = lines[line].split(';');
          var params = {
            state: info[0],
            province: info[1],
            city: info[2],
            latitude: parseFloat(info[3]),
            longitude: parseFloat(info[4]),
            name: info[5],
            address: info[6]
          };

          HealthUnit.create(params).exec((err, obj) => {
            if(err){
              console.log("Fail saving Health Unit: " + err);
              console.log("Params: " + params);
            }else{
              console.log("Success saving Health Unit: " + obj);
            }
          });
        }
      });
    });
    //[TODO] Convert to callback
    return res.redirect("/health_units");
  }
};
