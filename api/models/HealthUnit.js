/**
* HelthUnit.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  identity: "HealthUnit",
  attributes: {
    name: { type: 'string', required: true, unique: true},
    city : { type: 'string', required: true},
    province : { type: 'string', required: true},
    state : { type: 'string', required: true},
    latitude : { type: 'float', required: true},
    longitude : { type: 'float', required: true},
    address: {type: 'string', required: true},
  }
};
