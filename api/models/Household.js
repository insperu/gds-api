/**
 * HouseholdMember.js
 *
 * @description :: Describes a model for household member
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {
    attributes: {
        user: {model: 'User', required: true},
        firstname: {type: 'string', required: true},
        lastname: {type: 'string', required: false},
        gender: {type: 'string', enum: ['Masculino', 'Femenino'], required: true},
        dob: {type: 'date', required: true},
        country: {type: 'string', required: true},
        state: {type: 'string', required: false},
        province: {type: 'string', required: false},
        city: {type: 'string', required: false},
        relationship: {type: 'string', required: true},
        picture: {type: 'string', required: false},
        photo: {type: 'string', required: false},
        race: {type: 'string', required: true},
        surveys: {
            collection: 'Survey',
            via: 'household'
        }
    },
    beforeValidate: function (attr, next) {
        if (attr.picture == null || attr.picture == "") {
            attr.picture = 0;
        }
        if (attr.dob == null && attr.dob_month != null && attr.dob_year != null) {
            attr.dob = attr.dob_month + '/' + attr.dob_year;
            delete attr.dob_month;
            delete attr.dob_year;
        }
        if(attr.email == "" || attr.email == " ") {
            delete attr.email;
        }
        next();
    }
};
