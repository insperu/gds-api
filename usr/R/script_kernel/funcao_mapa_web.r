
##
gerawebmap <- function(local,sindrome,contorno,kernel,kilha=NULL,pontos=NULL) {
    require(rgdal)
    require(dplyr)
    require(leaflet)
    require(htmlwidgets) 
    require(raster)

    rkern <- raster(kernel)
    
    if (!is.null(kilha))   rilha <- raster(kilha)

#rkern <- stack(rkern,rilha)
    cores[1] <- "#E2E6BDA0"
    
box <- bbox(contorno)
m <- leaflet(contorno) %>% addTiles() %>%
    addRectangles(
        lng1=box[1,1], lat1=box[2,1],
        lng2=box[1,2], lat2=box[2,2],
        fillColor = "transparent", stroke=FALSE
    ) %>%
    addRasterImage(rkern, colors = cores, opacity = 0.8) 

if(!is.null(kilha)) m <- m %>% addRasterImage(rilha, colors = cores, opacity = 0.8) 

arq <- paste0(onde,local,'_',sindrome,'.html')
saveWidget(m, file=arq,selfcontained = TRUE, libdir = NULL)

}